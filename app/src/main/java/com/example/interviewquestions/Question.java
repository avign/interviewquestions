package com.example.interviewquestions;

public class Question {

    private String QuestionHeading="";
    private String Company="";
    private String College="";
    private String Difficulty="";
    private String QuestionUrl = "";


    public Question(String heading) {
        QuestionHeading = heading;
    }
    public Question(String questionHeading,String company,String college,String difficulty,String Url) {
        QuestionHeading  = questionHeading;
        Company = company;
        College = college;
        Difficulty = difficulty;
        QuestionUrl = Url;

    }

    public String getQuestionUrl() {
        return QuestionUrl;
    }

    public String getQuestionHeading() {
        return QuestionHeading;
    }

    public String getCollege() {
        return College;
    }

    public String getCompany() {
        return Company;
    }

    public String getDifficulty() {
        return Difficulty;
    }
}
