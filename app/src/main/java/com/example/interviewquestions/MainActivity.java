package com.example.interviewquestions;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintsChangedListener;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Question> filtered = new ArrayList<Question>();
    private ArrayList<String> cmpnyFilters = new ArrayList<String>();
    private ArrayList<String> diffFilters = new ArrayList<String>();
    private ArrayList<String> clgFilters = new ArrayList<String >();
    private QuestionAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getQuestions();
        ListView listView = (ListView) findViewById(R.id.list);

        CheckBox bhu = (CheckBox) findViewById(R.id.IIT_BHU);
        CheckBox delhi = (CheckBox) findViewById(R.id.IIT_D);
        CheckBox bombay = (CheckBox) findViewById(R.id.IIT_B);

        bhu.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    clgFilters.add("IIT BHU");
                else
                clgFilters.remove("IIT BHU");

                getQuestions();
                UpdateListView();
            }
        });
        delhi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    clgFilters.add("IIT DELHI");
                else
                    clgFilters.remove("IIT DELHI");
                getQuestions();
                UpdateListView();
            }
        });
        bombay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    clgFilters.add("IIT BOMBAY");
                else
                    clgFilters.remove("IIT BOMBAY");
            getQuestions();
            UpdateListView();
            }
        });

        CheckBox Jpmc = (CheckBox) findViewById(R.id.JP_Mogran);
        CheckBox Ally = (CheckBox) findViewById(R.id.Ally_io);
        CheckBox Amazon = (CheckBox) findViewById(R.id.Amazon);

        Jpmc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    cmpnyFilters.add("JP Morgan");
                else
                    cmpnyFilters.remove("JP Morgan");
            getQuestions();
            UpdateListView();
            }
        });

        Amazon.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    cmpnyFilters.add("Amazon");
                else
                    cmpnyFilters.remove("Amazon");
            getQuestions();
            UpdateListView();
            }
        });
        Ally.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    cmpnyFilters.add("Ally");
                else
                    cmpnyFilters.remove("Ally");
            getQuestions();
            UpdateListView();
            }
        });

        CheckBox hard = (CheckBox) findViewById(R.id.Hard);
        CheckBox easy = (CheckBox) findViewById(R.id.easy);
        CheckBox medium = (CheckBox) findViewById(R.id.Medium);

        hard.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    diffFilters.add("HARD");
                else
                    diffFilters.remove("HARD");
            getQuestions();
            UpdateListView();
            }
        });

        easy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    diffFilters.add("EASY");
                else
                    diffFilters.remove("EASY");
            getQuestions();
            UpdateListView();
            }
        });
        medium.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    diffFilters.add("MEDIUM");
                else
                    diffFilters.remove("MEDIUM");
            getQuestions();
            UpdateListView();
            }
        });
        QuestionAdapter adapter = new QuestionAdapter(this, filtered);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Question question = filtered.get(position);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(question.getQuestionUrl()));
                startActivity(intent);
            }
        });



    }
    public void getQuestions(){
        filtered = new ArrayList<Question>();
        ArrayList<Question> fillt = new ArrayList<Question>();
        for (int i=0;i<100;i++) {
            filtered.add(new Question("Lonest pallindrome Substring", "Amazon", "IIT BHU", "HARD", "https://www.geeksforgeeks.org/longest-palindrome-substring-set-1/"));
            filtered.add(new Question("Longest Path Length(Undirected Graph)","JP Morgan","IIT DELHI","MEDIUM","https://www.geeksforgeeks.org/longest-path-undirected-tree/"));
            filtered.add(new Question("Cherry pickup","Ally","IIT BOMBAY","HARD","https://leetcode.com/problems/cherry-pickup/"));
            filtered.add(new Question("Strong Password Checker","Ally","IIT BHU","EASY","https://leetcode.com/problems/strong-password-checker/"));
            filtered.add(new Question("Kth Ancestor of A Tree Node","JP Morgan","IIT DELHI","EASY","https://leetcode.com/problems/kth-ancestor-of-a-tree-node/"));
        }
        boolean[] valid = new boolean[filtered.size()];
        Arrays.fill(valid,Boolean.TRUE);

        if(cmpnyFilters.size()!=0){
            for(int i=0;i<filtered.size();i++){
                String cmpny = filtered.get(i).getCompany();
                boolean val= false;
                for(int j=0;j<cmpnyFilters.size();j++){
                    if(cmpnyFilters.get(j).equals(cmpny))
                        val = true;
                }
                if(!val)
                    valid[i] = false;
            }
        }
        if(clgFilters.size()!=0){
            for(int i=0;i<filtered.size();i++){
                String clg = filtered.get(i).getCollege();
                boolean val= false;
                for(int j=0;j<clgFilters.size();j++){
                    if(clgFilters.get(j)==clg)
                        val = true;
                }
                if(!val)
                    valid[i] = false;
            }
        }
        if(diffFilters.size()!=0){
            for(int i=0;i<filtered.size();i++){
                String diff = filtered.get(i).getDifficulty();
                boolean val= false;
                for(int j=0;j<diffFilters.size();j++){
                    if(diffFilters.get(j)==diff)
                        val = true;
                }
                if(!val)
                    valid[i] = false;
            }
        }
        for(int i=0;i<filtered.size();i++)
        {
            if(valid[i])
                fillt.add(filtered.get(i));
        }
        filtered = fillt;



    }

    public void UpdateListView()
    {
        ListView ListVw = (ListView) findViewById(R.id.list);
        if(ListVw != null)
        {
            QuestionAdapter adapter = (QuestionAdapter) ListVw.getAdapter();
            adapter.setList(filtered);
            adapter.notifyDataSetChanged();
        }
    }
}