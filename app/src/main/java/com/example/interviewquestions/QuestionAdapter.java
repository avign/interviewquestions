package com.example.interviewquestions;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.net.PortUnreachableException;
import java.util.ArrayList;

public class QuestionAdapter extends ArrayAdapter<Question> {
    public QuestionAdapter(Activity context, ArrayList<Question> questions) {
        // Here, we initialize the ArrayAdapter's internal storage for the context and the list.
        // the second argument is used when the ArrayAdapter is populating a single TextView.
        // Because this is a custom adapter for two TextViews and an ImageView, the adapter is not
        // going to use this second argument, so it can be any value. Here, we used 0.
        super(context, 0, questions);

    }
    public void setList(ArrayList<Question> filterd){
        this.clear();
        this.addAll(filterd);
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull  ViewGroup parent) {

        // Check if the existing view is being reused, otherwise inflate the view
        View listItemView = convertView;
        if(listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item, parent, false);
        }


        Question currentQuestion = getItem(position);


        TextView headingTextView = (TextView) listItemView.findViewById(R.id.heading_text_view);
        headingTextView.setText(currentQuestion.getQuestionHeading());

        TextView CompanyTextView = (TextView) listItemView.findViewById(R.id.company_text_view);
        CompanyTextView.setText(currentQuestion.getCompany());

        TextView DifficultyTextView = (TextView) listItemView.findViewById(R.id.difficulty_text_view);
        DifficultyTextView.setText(currentQuestion.getDifficulty());

        TextView CollegeTextView = (TextView) listItemView.findViewById(R.id.college_text_view);
        CollegeTextView.setText(currentQuestion.getCollege());



        return listItemView;
    }
}
